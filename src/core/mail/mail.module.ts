import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { config } from 'src/config/config';
import { UsersService } from '../user/services/user.service';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: config.nodeMailer.config,
    }),
  ],
  providers: [MailService, UsersService],
  exports: [MailService],
})
export class MailModule {}
