export class UserDto {
  username: string;
  email?: string;
  managet?: string;
  displayname?: string;
  firstName?: string;
  lastName?: string;
  roles?: string[];
}
