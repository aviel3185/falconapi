export class JwtPayload {
  username: string;
  roles: string[];
}
