export enum Roles {
  Unauthorized = 'unauthorized',
  Viewer = 'viewer',
  Admin = 'admin',
}
