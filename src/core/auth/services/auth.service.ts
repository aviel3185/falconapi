import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { UsersService } from 'src/core/user/services/user.service';
import { JwtService } from '@nestjs/jwt';
import { config } from 'src/config/config';
import { Roles } from '../enums/roles.enum';
import { LoggerService } from '../../../common/services/logger.service';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { AccessToken } from '../interfaces/access-token.interface';

@Injectable()
export class AuthService {
  private readonly logger = new LoggerService(AuthService.name);

  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  /**
   * Check if user token exist and valid.
   * @param payload
   */
  async validateUser(payload: JwtPayload): Promise<boolean> {
    return !!payload;
  }

  /**
   *
   * Authenticate user by creating a jwt token.
   * @param username
   */
  async authenticate(username: any): Promise<AccessToken> {
    try {
      const roles = await this.getUserRole(username);
      const payload: JwtPayload = { username: 'noa', roles: roles };
      return {
        accessToken: this.jwtService.sign(payload),
      };
    } catch (error) {
      this.logger.error(error.message, error.trace, 'authenticate');
      throw new InternalServerErrorException();
    }
  }

  private async getUserRole(username: string): Promise<string[]> {
    const roles = [];
    if (
      config.auth.adminGroups.some(
        async (x) => await this.usersService.isUserInGroup(username, x),
      ) ||
      config.auth.adminUsers.includes(username)
    ) {
      roles.push(Roles.Admin);
    }
    if (
      config.auth.viewerGroups.some(
        async (x) => await this.usersService.isUserInGroup(username, x),
      ) ||
      config.auth.viewerUsers.includes(username)
    ) {
      roles.push(Roles.Viewer);
    }

    return roles.length ? roles : [Roles.Unauthorized];
  }
}
