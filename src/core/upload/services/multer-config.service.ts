import { Injectable, BadRequestException } from '@nestjs/common';
import {
  MulterOptionsFactory,
  MulterModuleOptions,
} from '@nestjs/platform-express';
import { config } from 'src/config/config';
import { diskStorage } from 'multer';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  createMulterOptions(): MulterModuleOptions {
    return {
      fileFilter: this.fileTypeFilter,
      storage: diskStorage({
        filename: this.getFileName,
        destination: config.upload.destinationFolder,
      }),
      limits: {
        fileSize: config.upload.maxSize,
        files: config.upload.maxFiles,
      },
    };
  }

  private fileTypeFilter = (req, file, callback) => {
    console.log(config.upload.types);
    if (
      !config.upload.types.some((type) =>
        file.originalname.includes(`.${type}`),
      )
    ) {
      return callback(new BadRequestException('File type is not valid'), false);
    }
    return callback(null, true);
  };

  private getFileName = (req, file, callback) => {
    callback(null, file.originalname + '-' + Date.now());
  };
}
