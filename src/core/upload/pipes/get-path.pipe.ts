import { PipeTransform } from '@nestjs/common';
import { join } from 'path';
import { config } from 'src/config/config';

export class GetPathPipe implements PipeTransform {
  async transform(value: any) {
    const path = join(
      __dirname,
      '../../../../',
      `${config.upload.destinationFolder}/${value}`,
    );
    return path;
  }
}
