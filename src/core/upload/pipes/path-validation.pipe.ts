import * as fs from 'fs-extra';
import { PipeTransform, BadRequestException } from '@nestjs/common';
import { LoggerService } from 'src/common/services/logger.service';

export class PathValidationPipe implements PipeTransform {
  private readonly logger = new LoggerService(PathValidationPipe.name);

  async transform(value: any) {
    let fileValid: boolean;
    try {
      fileValid = await fs.pathExists(value);
    } catch (err) {
      this.logger.error(err, PathValidationPipe.name);
    }
    if (!fileValid) {
      throw new BadRequestException(`${value} not exist`);
    }
    return value;
  }
}
