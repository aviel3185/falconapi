import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './user/user.module';

@Module({})
export class CoreModule {
  imports: [UsersModule, AuthModule];
  exports: [UsersModule, AuthModule];
}
