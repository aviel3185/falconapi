import { GatewayService } from './gateway.service';
import { GatewayController } from './gateway.controller';
import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
    providers: [GatewayService],
    exports: [GatewayService],
    controllers: [GatewayController],
    imports:[
        ClientsModule.register([
            {
              name: "SERVICE_A",
              transport: Transport.TCP,
              options: {
                host: "127.0.0.1",
                port: 5001
              }
            }
          ]),
    ]
})
export class GatewayModule { }
