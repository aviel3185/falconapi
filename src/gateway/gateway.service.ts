import { ClientProxy } from '@nestjs/microservices';
import { Injectable, Inject } from "@nestjs/common";
import { map } from "rxjs/operators";

@Injectable()
export class GatewayService {
    constructor(@Inject('SERVICE_A') private readonly clientServiceA: ClientProxy) { }

    pingOne() {
        const startTs = Date.now();
        const pattern = { cmd: "ping" };
        const payload = {};
        return this.clientServiceA
            .send<string>(pattern, payload)
            .pipe(
                map((message: string) => ({ message, duration: Date.now() - startTs }))
            );
    }
}