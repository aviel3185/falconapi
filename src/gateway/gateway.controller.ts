import { GatewayService } from './gateway.service';
import { Controller, Get } from '@nestjs/common';

@Controller('hi')
export class GatewayController {
    constructor(private readonly gatewayService: GatewayService) { 

        console.log('hi')
    }
    @Get()
    ping() {
        return this.gatewayService.pingOne();
    }
}
