import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';
import { Types } from 'mongoose';
@Injectable()
export class ObjectIdValidatorPipe implements PipeTransform {
  transform(value: string) {
    if (!Types.ObjectId.isValid(value)) {
      throw new BadRequestException(`The id: "${value}" is not valid`);
    }

    return value;
  }
}
