import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { LoggerService } from '../services/logger.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  private readonly logger: LoggerService = new LoggerService(
    LoggerInterceptor.name,
  );

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const contextType = context.getType();
    const now = Date.now();

    return next.handle().pipe(
      tap(
        () => {
          if (contextType === 'http') {
            this.logHttpRequest(context, now);
          }
        },
        (error: Error) => {
          if (contextType === 'http') {
            this.logHttpRequest(context, now, error);
          }
        },
      ),
    );
  }

  private logHttpRequest(
    context: ExecutionContext,
    startTime: number,
    error?: Error,
  ) {
    if (context.getType() !== 'http') return;

    const httpRequest = context.switchToHttp().getRequest<Request>();
    const httpResponse = context.switchToHttp().getResponse<Response>();
    const logObject = {
      reqTime: Date.now(),
      controllerName: context.getClass().name,
      handlerName: context.getHandler().name,
      url: httpRequest.url,
      method: httpRequest.method,
      statusCode: httpResponse.status,
    };

    if (error) {
      this.logger.error(error);
    } else {
      this.logger.log(logObject);
    }
  }
}
