import { Injectable } from '@nestjs/common';
import { SocketioGateway } from './socketio.gateway';
import { LoggerService } from '../services/logger.service';

@Injectable()
export class SocketioService {
  private logger = new LoggerService(SocketioService.name);

  constructor(private readonly socketioGateway: SocketioGateway) {}

  public emitEvent(eventname: string, data): void {
    try {
      this.socketioGateway.server.emit(eventname, data);
    } catch (error) {
      this.logger.log(error, this.emitEvent.name);
    }
  }

  public emitEventToRoom(eventname: string, room: string, data): void {
    try {
      this.socketioGateway.server.to(room).emit(eventname, data);
    } catch (error) {
      this.logger.log(error, this.emitEvent.name);
    }
  }
}
