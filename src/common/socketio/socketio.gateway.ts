import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayDisconnect,
  OnGatewayConnection,
  OnGatewayInit,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { LoggerService } from '../services/logger.service';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/core/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/core/auth/guards/roles.guard';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from 'src/core/auth/interfaces/jwt-payload.interface';
import { config } from 'src/config/config';
import { UsersService } from 'src/core/user/services/user.service';
import { Roles } from 'src/core/auth/decorators/roles.decorator';

@UseGuards(JwtAuthGuard, RolesGuard)
@Roles('admin', 'viewer')
@WebSocketGateway()
export class SocketioGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  private logger = new LoggerService(SocketioGateway.name);
  @WebSocketServer() server;

  constructor(private usersSrervice: UsersService) {}

  afterInit() {
    this.logger.log('Socket initialized');
  }

  async handleConnection(client: Socket): Promise<void> {
    try {
      const payload = this.getPayload(client);
      const user = await this.usersSrervice.getUserDetails(payload);
      this.joinToGroups(client, payload);
      this.logger.log(`${user} client connected`);
    } catch (error) {
      this.logger.error(error, 'handleConnection');
    }
  }

  handleDisconnect(client: Socket): void {
    this.logger.log('client disconnect');
  }

  private joinToGroups(socket: Socket, user: JwtPayload): void {
    socket.join(user.username);

    user.roles.forEach((role) => socket.join(role));
  }

  private getPayload(client: Socket): JwtPayload {
    const authToken = client.handshake.headers.authorization.split(' ')[1];
    const jwtPayload = jwt.verify(authToken, config.auth.secret) as JwtPayload;
    return jwtPayload;
  }
}
