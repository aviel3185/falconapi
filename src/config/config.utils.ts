export enum Environment {
  Development = 'development',
  Production = 'production',
  Local = 'local',
}

export function boolean(variable: string): boolean {
  return variable === 'true';
}

export function integer(variable: string): number {
  return parseInt(variable, 10);
}

export function float(variable: string): number {
  return parseFloat(variable);
}

export function stringArray(variable: string): string[] {
  return variable ? variable.split(',').map((value) => value.trim()) : [];
}
