import { ValidationPipeOptions } from '@nestjs/common';
import { HandlebarsAdapter } from '@nestjs-modules/mailer';

export enum Environment {
  Development = 'development',
  Production = 'production',
  Local = 'local',
}

export interface Config {
  port: number;
  host: string;
  environment: Environment;
  socketIO: boolean;
  validation: ValidationPipeOptions;
  peopleapi: {
    key: string;
  };
  db: {
    uri: string;
    type: string;
    host: string;
    port: string;
    connectionString?: string;
    username?: string;
    password?: string;
    database?: string;
  };
  auth: {
    secret: string;
    viewerUsers: string[];
    viewerGroups: string[];
    privilagedUsers: string[];
    privilagedGroups: string[];
    adminUsers: string[];
    adminGroups: string[];
    tokenExpiry: string;
  };
  upload: {
    types: string[];
    maxFiles: number;
    maxSize: number;
    destinationFolder: string;
  };
  nodeMailer: {
    recipients: {
      users: string[];
      groups: string[];
    };
    config: {
      host: string;
      port: number;
      secure: boolean;
      auth: {
        user: string;
        pass: string;
      };
      tls: {
        rejectUnauthorized: boolean;
      };
    };
  };
}
