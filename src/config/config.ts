import defaultConfig from './default';
import productionConfig from './env/production';
import developmentConfig from './env/development';
import { Config } from './config.interface';
import * as merge from 'lodash.merge';

const environmentConfig = {
  development: developmentConfig,
  production: productionConfig,
  local: {},
};

export const config: Config = merge(
  defaultConfig,
  environmentConfig[defaultConfig.environment],
);
